extends Node2D

const IngredientDescription = preload("IngredientDescription.gd")
const ingredient_dict = IngredientDescription.ingredient_dict
var recipeList = []
var accessibility_mode = true
var settings_file = "user://settings.save"

var current_recipe = null
var start_title = null
var recipe = {
	"extra": [],
	"substitution": []
}

# Called when the node enters the scene tree for the first time.
func _ready():
	recipeList = $RecipeInfo.recipes
	connect_to_ingredients()
	setup_audio()
#	load_recipe("Pizza")
	disable_submit_button()
	var file = File.new()
	if file.file_exists(settings_file):
		file.open(settings_file, file.READ)
		self.accessibility_mode = file.get_var()

func remove_achievements_file():
	var dir = Directory.new()
	dir.remove("user://achievements.json")

func disable_submit_button():
	var submitButton = $Recipe/SubmitButton
	submitButton.disabled = true
	submitButton.get_node("Label").add_color_override("font_color", Color.dimgray)

func enable_submit_button():
	var submitButton = $Recipe/SubmitButton
	submitButton.disabled = false
	submitButton.get_node("Label").add_color_override("font_color", Color.white)

func connect_to_ingredients():
	var locations = ["Table", "Pantry", "Fridge"]
	for location in locations:
		for ingredient in get_tree().get_nodes_in_group("ingredient"):
			ingredient.connect("ingredient_click", self, "_on_ingredient_clicked")
			ingredient.connect("ingredient_mouseover", self, "_on_ingredient_mouseover")

func setup_audio():
	var locations = ["Table", "Pantry", "Fridge"]
	for ingredient in get_tree().get_nodes_in_group("ingredient"):
		$VaPlayer.add_sound(ingredient.get_name(), "res://assets/audio/va/Ingredients/" + ingredient.get_name() + ".wav")
		$AccessibilityPlayer.add_sound(ingredient.get_name(), "res://assets/audio/va/Ingredients/" + ingredient.get_name() + ".wav")
		$AccessibilityPlayer.add_sound("%s_title" % ingredient.get_name(), "res://assets/audio/va/Ingredients/%s_title.wav" % ingredient.get_name())
	var dir = Directory.new()
	var path = "res://assets/audio/va/RecipeInfo"
	dir.open(path)
	dir.list_dir_begin()
	while true:
		var file = dir.get_next()
		if file == "":
			break
		if file.ends_with(".wav.import"):
			var sound_name = file.replace(".wav.import", "")
			var path_name = "%s/%s" % [path, file.replace(".import", "")]
			$VaPlayer.add_sound(sound_name, path_name)
			$VaPlayer.add_sound(sound_name, path_name)
			$AccessibilityPlayer.add_sound(sound_name, path_name)
	var acc_dir = Directory.new()
	path = "res://assets/audio/va/accessibility"
	acc_dir.open(path)
	acc_dir.list_dir_begin()
	while true:
		var file = acc_dir.get_next()
		if file == "":
			break
		if file.ends_with(".wav.import"):
			$AccessibilityPlayer.add_sound(file.replace(".wav.import", ""), 
			"%s/%s" % [path, file.replace(".import", "")])
	var sfx_dir = Directory.new()
	path = "res://assets/audio/sfx"
	sfx_dir.open(path)
	sfx_dir.list_dir_begin()
	while true:
		var file = acc_dir.get_next()
		if file == "":
			break
		if file.ends_with(".wav.import"):
			$SFXPlayer.add_sound(file.replace(".wav.import", ""), 
			"%s/%s" % [path, file.replace(".import", "")])

func _on_ingredient_clicked(item_name):
	$IngredientPopup/IngredientName.text = item_name
	$IngredientPopup/IngredientDescription.text = ingredient_dict[item_name]["description"]
	$IngredientPopup.popup_centered()
	$VaPlayer.play_sound(item_name)

func _on_ingredient_mouseover(item_name):
	if self.accessibility_mode:
		$AccessibilityPlayer.play_sound("%s_title" % item_name)

func _on_FridgeArea_mouse_entered():
	if self.accessibility_mode and !($"Table View".visible or $"Fridge View".visible or $"Pantry View".visible):
		$AccessibilityPlayer.play_sound("fridge")

func _on_FridgeArea_input_event(viewport, event, shape_idx):
	if (event is InputEventMouseButton \
		and event.button_index == BUTTON_LEFT \
		and event.is_pressed()) or (Input.is_action_just_pressed("ui_accept")):
			self.open_fridge()

func _on_PantryArea_mouse_entered():
	if self.accessibility_mode and !($"Table View".visible or $"Fridge View".visible or $"Pantry View".visible):
		$AccessibilityPlayer.play_sound("pantry")

func _on_PantryArea_input_event(viewport, event, shape_idx):
	if (event is InputEventMouseButton \
		and event.button_index == BUTTON_LEFT \
		and event.is_pressed()) or (Input.is_action_just_pressed("ui_accept")):
			self.open_pantry()

func _on_TableArea_mouse_entered():
	if self.accessibility_mode and !($"Table View".visible or $"Fridge View".visible or $"Pantry View".visible):
		$AccessibilityPlayer.play_sound("table")

func _on_TableArea_input_event(viewport, event, shape_idx):
	if (event is InputEventMouseButton \
		and event.button_index == BUTTON_LEFT \
		and event.is_pressed()) or (Input.is_action_just_pressed("ui_accept")):
			self.open_table()

func open_fridge():
	if !$"Table View".visible and !$"Pantry View".visible:
		$"Fridge View".show()
		$RecipeButton.hide()

func open_pantry():
	if !$"Table View".visible and !$"Fridge View".visible:
		$"Pantry View".show()
		$RecipeButton.hide()
		
func open_table():
	if !$"Fridge View".visible and !$"Pantry View".visible:
		$"Table View".show()
		$RecipeButton.hide()

func _on_CloseTableButton_pressed():
	$"Table View".hide()
	$RecipeButton.show()

func _on_ClosePantryButton_pressed():
	$"Pantry View".hide()
	$RecipeButton.show()

func _on_CloseFridgeButton_pressed():
	$"Fridge View".hide()
	$RecipeButton.show()

func _on_CancelButton_pressed():
	$IngredientPopup.hide()

func _on_AddButton_pressed():
	self.add_to_recipe($IngredientPopup/IngredientName.text)
	$IngredientPopup.hide()

func add_to_recipe(item_name):
	self.recipe["extra"].append(item_name)
	var item = self.get_ingredient(item_name)
	item.hide()
	$SFXPlayer.play_sound("check")

func get_ingredient(item_name):
	var item = null
	for ingredient in get_tree().get_nodes_in_group("ingredient"):
		if ingredient.get_name() == item_name:
			item = ingredient
			break
	return item

func show_ingredients(to_hide=null):
	if !to_hide:
		to_hide = []
	var item_name = null
	for ingredient in get_tree().get_nodes_in_group("ingredient"):
		item_name = ingredient.get_name()
		if item_name in to_hide:
			ingredient.hide()
		else:
			ingredient.show()

func load_recipe(recipe_name):
	for item in recipeList:
		if item["title"] == recipe_name:
			self.current_recipe = item
			break
	var to_hide = self.current_recipe["existing"] + [self.current_recipe["missing"]]
	if self.current_recipe.has("remove"):
		to_hide += self.current_recipe["remove"]
	self.show_ingredients(to_hide)
	
	self.recipe["extra"].clear()
	self.recipe["substitution"].clear()
	$Recipe/VBoxContainer/RecipeLabel.text = recipe_name
	$Recipe/VBoxContainer/BaseLabel.text = "Basic: %s" % PoolStringArray(self.current_recipe["existing"]).join(", ")
	$Recipe/VBoxContainer/MissingLabel.text = "Missing: %s" % self.current_recipe["missing"]
	
	$LevelStart/VBoxContainer/RecipeLabel.text = recipe_name
	$LevelStart/VBoxContainer/MissingLabel.text = "Missing: %s" % self.current_recipe["missing"]
	$LevelStart/VBoxContainer/CommentLabel.text = "Comment: %s" % self.current_recipe["start"]
	$LevelStart.popup_centered()
	if self.accessibility_mode:
		var sound_names = [self.current_recipe["title"], "missing", "%s_title" % self.current_recipe["missing"], 
		"%s_start" % self.current_recipe["title"]]
		$AccessibilityPlayer.play_sounds(sound_names)
	else:
		print($VaPlayer.sounds["%s_start" % self.current_recipe["title"]])
		$VaPlayer.play_sound("%s_start" % self.current_recipe["title"])

func open_recipe_menu():
	$Recipe/VBoxContainer/ExtraLabel.text = "Extra: %s" % PoolStringArray(self.recipe["extra"]).join(", ")
	$Recipe/VBoxContainer/SubLabel.text = "Substitution: %s" % PoolStringArray(self.recipe["substitution"]).join(", ")
	$Recipe.show()
	if self.accessibility_mode:
		var sound_names = [self.current_recipe["title"], "basic"]
		for item in self.current_recipe["existing"]:
			sound_names.append("%s_title" % item)
		sound_names.append("missing")
		sound_names.append("%s_title" % self.current_recipe["missing"])
		sound_names.append("extra")
		for item in self.recipe["extra"]:
			sound_names.append("%s_title" % item)
		sound_names.append("substitution")
		for item in self.recipe["substitution"]:
			sound_names.append("%s_title" % item)
		$AccessibilityPlayer.play_sounds(sound_names)
		
func _on_RecipeButton_pressed():
	self.open_recipe_menu()

func _on_CloseRecipeButton_pressed():
	$Recipe.hide()

func _on_RemoveButton_pressed():
	var removeMenu = $Recipe/RemoveMenu
	removeMenu.clear()
	for item in self.recipe["extra"]:
		removeMenu.add_item(item)
	for item in self.recipe["substitution"]:
		removeMenu.add_item(item)
	removeMenu.popup_centered()
	$SFXPlayer.play_sound("scribble")

func _on_RemoveMenu_id_pressed(id):
	var text = $Recipe/RemoveMenu.get_item_text(id)
	var index = self.recipe["extra"].find(text)
	if index != -1:
		self.recipe["extra"].remove(index)
		$Recipe/VBoxContainer/ExtraLabel.text = "Extra: %s" % PoolStringArray(self.recipe["extra"]).join(", ")
	else:
		index = self.recipe["substitution"].find(text)
		self.recipe["substitution"].remove(index)
		$Recipe/VBoxContainer/SubLabel.text = "Substitution: %s" % PoolStringArray(self.recipe["substitution"]).join(", ")
	self.get_ingredient(text).show()
	if self.recipe["substitution"].size() == 0:
		disable_submit_button()
	else:
		enable_submit_button()

func _on_SubButton_pressed():
	var subMenu = $Recipe/SubMenu
	subMenu.clear()
	for item in self.recipe["extra"]:
		subMenu.add_check_item(item)
	for item in self.recipe["substitution"]:
		subMenu.add_check_item(item)
		var id = subMenu.get_item_count()
		subMenu.set_item_checked(id - 1, true)
	subMenu.popup_centered()

func _on_SubMenu_id_pressed(id):
	var subMenu = $Recipe/SubMenu
	var text = subMenu.get_item_text(id)
	if text in self.recipe["extra"]:
		self.recipe["extra"].remove(self.recipe["extra"].find(text))
		self.recipe["substitution"].append(text)
		subMenu.set_item_checked(id, true)
	else:
		self.recipe["substitution"].remove(self.recipe["substitution"].find(text))
		self.recipe["extra"].append(text)
		subMenu.set_item_checked(id, false)
	$Recipe/VBoxContainer/SubLabel.text = "Substitution: %s" % PoolStringArray(self.recipe["substitution"]).join(", ")
	$Recipe/VBoxContainer/ExtraLabel.text = "Extra: %s" % PoolStringArray(self.recipe["extra"]).join(", ")
	if self.recipe["substitution"].size() == 0:
		disable_submit_button()
	else:
		enable_submit_button()
	$SFXPlayer.play_sound("circle")

func _on_SubmitButton_pressed():
	var result = $RecipeInfo.evaluate_recipe(self.current_recipe["title"], self.recipe["extra"], self.recipe["substitution"])
	var evalMenu = $Evaluation
	evalMenu.get_node("VBoxContainer/RecipeLabel").text = "%s: %s" % ["Success" if result["sub_ok"] else "Failure", self.current_recipe["title"]]
	evalMenu.get_node("VBoxContainer/GoodExtrasLabel").text = "Good Extras: %s" % PoolStringArray(result["good_extras"]).join(", ")
	evalMenu.get_node("VBoxContainer/BadExtrasLabel").text = "Bad Extras: %s" % PoolStringArray(result["bad_extras"]).join(", ")
	evalMenu.get_node("VBoxContainer/SubLabel").text = "Substitution: %s" % PoolStringArray(self.recipe["substitution"]).join(", ")
	var comment = ""
	if result["comment"]:
		comment = result["comment"]
	elif result["hint"]:
		comment = result["hint"]
	evalMenu.get_node("VBoxContainer/Commentlabel").text = "Comment: %s" % comment
	$Recipe.hide()
	evalMenu.show()
	if result["audio_name"] and !self.accessibility_mode:
		print("vaplayer playing %s" % result["audio_name"])
		$VaPlayer.play_sound(result["audio_name"])
	else:
		var sound_names = [self.current_recipe["title"], "good_extras"]
		for item in result["good_extras"]:
			sound_names.append("%s_title" % item)
		sound_names.append("bad_extras")
		for item in result["bad_extras"]:
			sound_names.append("%s_title" % item)
		sound_names.append("substitution")
		for item in self.recipe["substitution"]:
			sound_names.append("%s_title" % item)
		if result["audio_name"]:
			sound_names.append(result["audio_name"])
		$AccessibilityPlayer.play_sounds(sound_names)
	if result["sub_ok"]:
		$SFXPlayer.play_sound("check")
	else:
		$SFXPlayer.play_sound("crumple_paper")

func _on_RetryButton_pressed():
	$Evaluation.hide()
	self.load_recipe(self.current_recipe["title"])

func _on_NextRecipeButton_pressed():
	$Evaluation.hide()
	var index = recipeList.find(self.current_recipe)
	self.load_recipe(recipeList[index + 1]["title"])
	if index == recipeList.size() - 2:
		$Evaluation/NextRecipeButton.hide()

func _on_StartButton_pressed():
	get_tree().change_scene("res://scenes/Start.tscn")
	get_tree().get_root().remove_child(self)

func _on_CloseLevelStartButton_pressed():
	$LevelStart.hide()

func _on_RecipeButton_mouse_entered():
	if self.accessibility_mode:
		$AccessibilityPlayer.play_sound("recipe")

func _on_StartButton_mouse_entered():
	if self.accessibility_mode:
		$AccessibilityPlayer.play_sound("back_to_start")

func _on_CancelButton_mouse_entered():
	if self.accessibility_mode:
		$AccessibilityPlayer.play_sound("cancel")

func _on_AddButton_mouse_entered():
	if self.accessibility_mode:
		$AccessibilityPlayer.play_sound("add_to_recipe")

func _on_CloseRecipeButton_mouse_entered():
	if self.accessibility_mode:
		$AccessibilityPlayer.play_sound("close_menu")

func _on_RemoveButton_mouse_entered():
	if self.accessibility_mode:
		$AccessibilityPlayer.play_sound("remove_item")

func _on_SubButton_mouse_entered():
	if self.accessibility_mode:
		$AccessibilityPlayer.play_sound("make_substitution")

func _on_SubmitButton_mouse_entered():
	if self.accessibility_mode:
		$AccessibilityPlayer.play_sound("submit")

func _on_RemoveMenu_id_focused(id):
	if self.accessibility_mode:
		$AccessibilityPlayer.play_sound("%s_title" % $Recipe/RemoveMenu.get_item_text(id))

func _on_SubMenu_id_focused(id):
	if self.accessibility_mode:
		$AccessibilityPlayer.play_sound("%s_title" % $Recipe/SubMenu.get_item_text(id))

func _on_RetryButton_mouse_entered():
	if self.accessibility_mode:
		$AccessibilityPlayer.play_sound("retry")

func _on_NextRecipeButton_mouse_entered():
	if self.accessibility_mode:
		$AccessibilityPlayer.play_sound("next_recipe")

func _on_CloseTableButton_mouse_entered():
	if self.accessibility_mode and $"Table View/CloseTableButton".visible:
		$AccessibilityPlayer.play_sound("close_menu")

func _on_ClosePantryButton_mouse_entered():
	if self.accessibility_mode and $"Pantry View/ClosePantryButton".visible:
		$AccessibilityPlayer.play_sound("close_menu")

func _on_CloseFridgeButton_mouse_entered():
	if self.accessibility_mode and $"Fridge View/CloseFridgeButton".visible:
		$AccessibilityPlayer.play_sound("close_menu")
