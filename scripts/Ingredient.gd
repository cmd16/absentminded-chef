extends Area2D

class_name Ingredient

signal ingredient_click(name)
signal ingredient_mouseover(name)

func _input_event(viewport, event, shape_idx):
	if event is InputEventMouseButton \
	and event.button_index == BUTTON_LEFT \
	and event.is_pressed():
		self.on_click()

func on_click():
	emit_signal("ingredient_click", self.get_name())

func _on_Ingredient_mouse_entered():
	emit_signal("ingredient_mouseover", self.get_name())
