extends Control

const achievements_panel_scene = preload("res://scenes/achievements_panel.tscn")
var regex = RegEx.new()
var recipe_order = ["pizza", "soup", "sandwich", "spaghetti", "omelette", "sushi", "macandcheese"]
var other_order = ["oregano", "ingredients", "extras", "nonfood", "combination", "subs"]
var show_hidden_achievements = false

# Called when the node enters the scene tree for the first time.
func _ready():
	regex.compile("^([a-z]+)_([a-z]+)")
	display_achievements()

func display_achievements():
	for node in $ScrollContainer/VBoxContainer.get_children():
		node.queue_free()
	var achievement_names = AchievementManager.get_all_achievements().keys()
	achievement_names.sort_custom(self, "custom_achievement_sorter")
	for achievement_name in achievement_names:
		var achievement = AchievementManager.get_achievement(achievement_name)
		var achievement_panel = achievements_panel_scene.instance()
		achievement_panel.set_achievement(achievement)
		if achievement["achieved"] or self.show_hidden_achievements or !achievement["is_secret"]:
			achievement_panel.show()
		else:
			achievement_panel.hide()
		$ScrollContainer/VBoxContainer.add_child(achievement_panel)

func custom_achievement_sorter(a, b):
	var match_a = regex.search(a)
	var match_a1 = match_a.get_string(1)
	var match_b = regex.search(b)
	var match_b1 = match_b.get_string(1)
	if match_a1 != match_b1:
		if match_a1 == "other":
			return false
		if match_b1 == "other":
			return true
		return recipe_order.find(match_a1) < recipe_order.find(match_b1)
	var match_a2 = match_a.get_string(2)
	var match_b2 = match_b.get_string(2)
	if match_a1 == "other":
		return other_order.find(match_a2) < other_order.find(match_b2)
	if match_a2 == "sub":
		if match_b2 == "sub":
			var achievement_a = AchievementManager.get_achievement(a)
			var achievement_b = AchievementManager.get_achievement(b)
			return achievement_a["name"] < achievement_b["name"]
		return true  # sub comes before extras or subs
	if match_b2 == "sub":  # match_a2 is not sub
		return false  # sub comes before extras or subs
	return match_a2 == "extras"  # extras comes before subs

func _on_BackButton_pressed():
	get_tree().change_scene("res://scenes/Start.tscn")

func _on_CheckBox_pressed():
	self.show_hidden_achievements = !self.show_hidden_achievements
	display_achievements()
