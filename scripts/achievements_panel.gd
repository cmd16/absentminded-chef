extends Control

func set_achievement(achievement):
	$achievementPanel/mainTitle.text = achievement["name"]
	$achievementPanel/achievementDescription/description.text = achievement["description"]
	if "goal" in achievement:
		$achievementPanel/achievementDescription/description.text += " (%s/%s)" % [achievement["current_progress"], achievement["goal"]]
	var textureRect = get_node("achievementPanel/achievementIcon/TextureRect")
	if achievement["achieved"]:
		textureRect.texture = load(achievement["icon_path"])
	else:
		textureRect.texture = load("res://assets/art/ui/noun-square-question-617809.png")
		if achievement["is_secret"]:
			$achievementPanel/achievementDescription/description.text = "????????????????"

