extends Control

var settings_file = "user://settings.save"
var focused_button = null

var accessibility_mode = true
var music_volume = 3
var voice_volume = 3
var sfx_volume = 3

onready var musicMute = $VBoxContainer/MusicVolumeBox/MuteButton
onready var music1 = $VBoxContainer/MusicVolumeBox/Volume1Button
onready var music2 = $VBoxContainer/MusicVolumeBox/Volume2Button
onready var music3 = $VBoxContainer/MusicVolumeBox/VolumeFullButton
onready var voiceMute = $VBoxContainer/VoiceVolumeBox/MuteButton
onready var voice1 = $VBoxContainer/VoiceVolumeBox/Volume1Button
onready var voice2 = $VBoxContainer/VoiceVolumeBox/Volume2Button
onready var voice3 = $VBoxContainer/VoiceVolumeBox/VolumeFullButton
onready var SFXMute = $VBoxContainer/SFXVolumeBox/MuteButton
onready var SFX1 = $VBoxContainer/SFXVolumeBox/Volume1Button
onready var SFX2 = $VBoxContainer/SFXVolumeBox/Volume2Button
onready var SFX3 = $VBoxContainer/SFXVolumeBox/VolumeFullButton

onready var numToButton = {
	0: {
		"music_show": musicMute,
		"music_hide": [music1, music2, music3],
		"voice_show": voiceMute,
		"voice_hide": [voice1, voice2, voice3],
		"sfx_show": SFXMute,
		"sfx_hide": [SFX1, SFX2, SFX3]
	},
	1: {
		"music_show": music1,
		"music_hide": [musicMute, music2, music3],
		"voice_show": voice1,
		"voice_hide": [voiceMute, voice2, voice3],
		"sfx_show": SFX1,
		"sfx_hide": [SFXMute, SFX2, SFX3]
	},
	2: {
		"music_show": music2,
		"music_hide": [music1, musicMute, music3],
		"voice_show": voice2,
		"voice_hide": [voice1, voiceMute, voice3],
		"sfx_show": SFX2,
		"sfx_hide": [SFX1, SFXMute, SFX3]
	},
	3: {
		"music_show": music3,
		"music_hide": [music1, music2, musicMute],
		"voice_show": voice3,
		"voice_hide": [voice1, voice2, voiceMute],
		"sfx_show": SFX3,
		"sfx_hide": [SFX1, SFX2, SFXMute]
	},
}

func _ready():
#	var dir = Directory.new()
#	dir.remove(settings_file)
	load_settings_file()
	for sound_name in ["settings", "accessibility_mode", "checked", "unchecked", 
	"music_volume", "mute", "volume1", "volume2", "full_volume", "voice_volume",
	"sfx_volume", "close_menu", "controls", "directions_controls", "accept_controls",
	"close_menu_controls"]:
		$AudioStreamPlayer.add_sound(sound_name, "res://assets/audio/va/accessibility/%s.wav" % sound_name)
	if self.accessibility_mode and self.visible:
		$AudioStreamPlayer.play_sound("settings")
		self.play_current_settings(true)
	setup_volume()

func play_current_settings(wait=false):
	if wait:
		yield(get_tree().create_timer(1), "timeout")
	var sound_names = ["accessibility_mode", "checked", "music_volume"]
	if self.music_volume == 0:
		sound_names.append("mute")
	elif self.music_volume == 1:
		sound_names.append("volume1")
	elif self.music_volume == 2:
		sound_names.append("volume2")
	else:
		sound_names.append("full_volume")
	sound_names.append("voice_volume")
	if self.voice_volume == 0:
		sound_names.append("mute")
	elif self.voice_volume == 1:
		sound_names.append("volume1")
	elif self.voice_volume == 2:
		sound_names.append("volume2")
	else:
		sound_names.append("full_volume")
	sound_names.append("sfx_volume")
	if self.sfx_volume == 0:
		sound_names.append("mute")
	elif self.sfx_volume == 1:
		sound_names.append("volume1")
	elif self.sfx_volume == 2:
		sound_names.append("volume2")
	else:
		sound_names.append("full_volume")
	sound_names.append("controls")
	sound_names.append("directions_controls")
	sound_names.append("accept_controls")
	sound_names.append("close_menu_controls")
	$AudioStreamPlayer.play_sounds(sound_names)

func _on_AccessibilityCheckBox_toggled(button_pressed):
	self.accessibility_mode = button_pressed

func setup_volume():
	self.numToButton[self.music_volume]["music_show"].emit_signal("pressed")
	self.numToButton[self.voice_volume]["voice_show"].emit_signal("pressed")
	self.numToButton[self.sfx_volume]["sfx_show"].emit_signal("pressed")

func setup_music_volume():
	self.numToButton[self.music_volume]["music_show"].modulate.a = 1
	for button in self.numToButton[self.music_volume]["music_hide"]:
		button.modulate.a = 0.25

func setup_voice_volume():
	self.numToButton[self.voice_volume]["voice_show"].modulate.a = 1
	for button in self.numToButton[self.voice_volume]["voice_hide"]:
		button.modulate.a = 0.25

func setup_sfx_volume():
	self.numToButton[self.sfx_volume]["sfx_show"].modulate.a = 1
	for button in self.numToButton[self.sfx_volume]["sfx_hide"]:
		button.modulate.a = 0.25

func _on_MusicMuteButton_pressed():
	AudioServer.set_bus_mute(1, true)
	self.music_volume = 0
	self.setup_music_volume()

func _on_MusicVolume1Button_pressed():
	AudioServer.set_bus_mute(1, false)
	AudioServer.set_bus_volume_db(1, -24)
	self.music_volume = 1
	self.setup_music_volume()

func _on_MusicVolume2Button_pressed():
	AudioServer.set_bus_mute(1, false)
	AudioServer.set_bus_volume_db(1, -18)
	self.music_volume = 2
	self.setup_music_volume()

func _on_MusicVolumeFullButton_pressed():
	AudioServer.set_bus_mute(1, false)
	AudioServer.set_bus_volume_db(1, -12)
	self.music_volume = 3
	self.setup_music_volume()

func _on_VoiceMuteButton_pressed():
	AudioServer.set_bus_mute(2, true)
	self.voice_volume = 0
	self.setup_voice_volume()

func _on_VoiceVolume1Button_pressed():
	AudioServer.set_bus_mute(2, false)
	AudioServer.set_bus_volume_db(2, -12)
	self.voice_volume = 1
	self.setup_voice_volume()

func _on_VoiceVolume2Button_pressed():
	AudioServer.set_bus_mute(2, false)
	AudioServer.set_bus_volume_db(1, -6)
	self.voice_volume = 2
	self.setup_voice_volume()

func _on_VoiceVolumeFullButton_pressed():
	AudioServer.set_bus_mute(2, false)
	AudioServer.set_bus_volume_db(2, -12)
	self.voice_volume = 3
	self.setup_voice_volume()

func _on_SFXMuteButton_pressed():
	AudioServer.set_bus_mute(3, true)
	self.sfx_volume = 0
	self.setup_sfx_volume()

func _on_SFXVolume1Button_pressed():
	AudioServer.set_bus_mute(3, false)
	AudioServer.set_bus_volume_db(3, -12)
	self.sfx_volume = 1
	self.setup_sfx_volume()

func _on_SFXVolume2Button_pressed():
	AudioServer.set_bus_mute(3, false)
	AudioServer.set_bus_volume_db(3, -6)
	self.sfx_volume = 2
	self.setup_sfx_volume()

func _on_SFXVolumeFullButton_pressed():
	AudioServer.set_bus_mute(3, false)
	AudioServer.set_bus_volume_db(3, 0)
	self.sfx_volume = 3
	self.setup_sfx_volume()

func update_settings_file():
	var file = File.new()
	file.open(settings_file, File.WRITE)
	file.store_var(self.accessibility_mode)
	file.store_8(music_volume)
	file.store_8(voice_volume)
	file.store_8(sfx_volume)
	file.close()

func load_settings_file():
	var file = File.new()
	if file.file_exists(settings_file):
		file.open(settings_file, File.READ)
		self.accessibility_mode = file.get_var()
		self.music_volume = file.get_8()
		self.voice_volume = file.get_8()
		self.sfx_volume = file.get_8()
		file.close()
	else:
		self.accessibility_mode = false
		self.music_volume = 2
		self.voice_volume = 3
		self.sfx_volume = 0
	$VBoxContainer/AccesibilityBox/AccessibilityCheckBox.pressed = self.accessibility_mode

func _on_Settings_hide():
	$AudioStreamPlayer.stop()
	self.update_settings_file()
	if self.get_parent().get_name() == "SettingsParent":
		self.get_parent().hide()

func _on_CloseButton_pressed():
	self.hide()

func move_to_node_center(node):
	self.focused_button = node
	var size = node.get_size()
	var new_loc = node.get_global_transform().get_origin() + (size / 2)
	self.get_viewport().warp_mouse(new_loc)
	node.emit_signal("mouse_entered")

func _process(delta):
	if Input.is_action_just_pressed("ui_down") or Input.is_action_just_pressed("ui_right") \
	or Input.is_action_just_pressed("ui_up") or Input.is_action_just_pressed("ui_left"):
		var node = null
		if !self.focused_button:
			node = $VBoxContainer/AccesibilityBox/AccessibilityCheckBox
		elif self.focused_button.get_name() == "AccessibilityCheckBox":
			if Input.is_action_just_pressed("ui_right") or Input.is_action_just_pressed("ui_up"):
				node = $CloseButton
			else:
				node = $VBoxContainer/MusicVolumeBox/VolumeFullButton
		elif self.focused_button.get_name() == "CloseButton":
			if Input.is_action_just_pressed("ui_down") or Input.is_action_just_pressed("ui_left"):
				node = $VBoxContainer/AccesibilityBox/AccessibilityCheckBox
			else:
				if self.accessibility_mode and self.visible:
					node = $VBoxContainer/ControlsGrid
				else:
					node = $VBoxContainer/SFXVolumeBox/MuteButton
		elif self.focused_button.get_name() == "ControlsGrid":
			if Input.is_action_just_pressed("ui_left") or Input.is_action_just_pressed("ui_up"):
				node = $VBoxContainer/SFXVolumeBox/VolumeFullButton
			else:
				node = $CloseButton
		elif Input.is_action_just_pressed("ui_left"):
			var idx = self.focused_button.get_parent().get_children().find(self.focused_button)
			var next_idx = null
			if idx == 1:
				next_idx = 4
			else:
				next_idx = idx - 1
			node = self.focused_button.get_parent().get_child(next_idx)
		elif Input.is_action_just_pressed("ui_right"):
			var idx = self.focused_button.get_parent().get_children().find(self.focused_button)
			var next_idx = null
			if idx == 4:
				next_idx = 1
			else:
				next_idx = idx + 1
			node = self.focused_button.get_parent().get_child(next_idx)
		elif self.accessibility_mode and self.visible and ((self.focused_button.get_parent().get_name() == "SFXVolumeBox" \
		and Input.is_action_just_pressed("ui_down")) \
		or self.focused_button.get_name() == "close_button"):
			if self.focused_button.get_name() == "close_button":
				if Input.is_action_just_pressed("ui_left") or Input.is_action_just_pressed("ui_up"):
					node = $VBoxContainer/AccesibilityBox/AccessibilityCheckBox
				else:
					node = $VBoxContainer/ControlsGrid
			else:
				node = $VBoxContainer/ControlsGrid
		else:
			if self.focused_button.get_parent().get_name() == "MusicVolumeBox" and Input.is_action_just_pressed("ui_up"):
				node = $VBoxContainer/AccesibilityBox/AccessibilityCheckBox
			elif self.focused_button.get_parent().get_name() == "SFXVolumeBox" and Input.is_action_just_pressed("ui_down"):
				node = $CloseButton
			elif self.focused_button.get_parent().get_name() == "VoiceVolumeBox" and Input.is_action_just_pressed("ui_up"):
				node = $VBoxContainer/MusicVolumeBox.get_child($VBoxContainer/VoiceVolumeBox.get_children().find(self.focused_button))
			elif (self.focused_button.get_parent().get_name() == "MusicVolumeBox" and Input.is_action_just_pressed("ui_down")) \
			or (self.focused_button.get_parent().get_name() == "SFXVolumeBox" and Input.is_action_just_pressed("ui_up")):
				node = $VBoxContainer/VoiceVolumeBox.get_child(self.focused_button.get_parent().get_children().find(self.focused_button))
			elif self.focused_button.get_parent().get_name() == "VoiceVolumeBox" and Input.is_action_just_pressed("ui_down"):
				node = $VBoxContainer/SFXVolumeBox.get_child($VBoxContainer/VoiceVolumeBox.get_children().find(self.focused_button))
		move_to_node_center(node)
	else:
		if Input.is_action_just_pressed("ui_accept") and self.focused_button:
			if self.focused_button == $VBoxContainer/AccesibilityBox/AccessibilityCheckBox:
				self.focused_button.pressed = !self.focused_button.pressed
			else:
				self.focused_button.emit_signal("pressed")

func _on_AccessibilityCheckBox_mouse_entered():
	self.focused_button = $VBoxContainer/AccesibilityBox/AccessibilityCheckBox
	if self.accessibility_mode and self.visible:
		$AudioStreamPlayer.play_sounds(["accessibility_mode", "checked"])

func _on_AccessibilityCheckBox_mouse_exited():
	self.focused_button = null

func _on_MusicMuteButton_mouse_entered():
	self.focused_button = $VBoxContainer/MusicVolumeBox/MuteButton
	if self.accessibility_mode and self.visible:
		$AudioStreamPlayer.play_sounds(["music_volume", "mute"])

func _on_MuteButton_mouse_exited():
	self.focused_button = null

func _on_MusicVolume1Button_mouse_entered():
	self.focused_button = $VBoxContainer/MusicVolumeBox/Volume1Button
	if self.accessibility_mode and self.visible:
		$AudioStreamPlayer.play_sounds(["music_volume", "volume1"])

func _on_Volume1Button_mouse_exited():
	self.focused_button = null

func _on_MusicVolume2Button_mouse_entered():
	self.focused_button = $VBoxContainer/MusicVolumeBox/Volume2Button
	if self.accessibility_mode and self.visible:
		$AudioStreamPlayer.play_sounds(["music_volume", "volume2"])

func _on_Volume2Button_mouse_exited():
	self.focused_button = null

func _on_MusicVolumeFullButton_mouse_entered():
	self.focused_button = $VBoxContainer/MusicVolumeBox/VolumeFullButton
	if self.accessibility_mode and self.visible:
		$AudioStreamPlayer.play_sounds(["music_volume", "full_volume"])

func _on_VolumeFullButton_mouse_exited():
	self.focused_button = null

func _on_VoiceMuteButton_mouse_entered():
	self.focused_button = $VBoxContainer/VoiceVolumeBox/MuteButton
	if self.accessibility_mode and self.visible:
		$AudioStreamPlayer.play_sounds(["voice_volume", "mute"])

func _on_VoiceVolume1Button_mouse_entered():
	self.focused_button = $VBoxContainer/VoiceVolumeBox/Volume1Button
	if self.accessibility_mode and self.visible:
		$AudioStreamPlayer.play_sounds(["voice_volume", "volume1"])

func _on_VoiceVolume2Button_mouse_entered():
	self.focused_button = $VBoxContainer/VoiceVolumeBox/Volume2Button
	if self.accessibility_mode and self.visible:
		$AudioStreamPlayer.play_sounds(["voice_volume", "volume2"])

func _on_VoiceVolumeFullButton_mouse_entered():
	self.focused_button = $VBoxContainer/VoiceVolumeBox/VolumeFullButton
	if self.accessibility_mode and self.visible:
		$AudioStreamPlayer.play_sounds(["voice_volume", "full_volume"])

func _on_SFXMuteButton_mouse_entered():
	self.focused_button = $VBoxContainer/SFXVolumeBox/MuteButton
	if self.accessibility_mode and self.visible:
		$AudioStreamPlayer.play_sounds(["sfx_volume", "mute"])

func _on_SFXVolume1Button_mouse_entered():
	self.focused_button = $VBoxContainer/SFXVolumeBox/Volume1Button
	if self.accessibility_mode and self.visible:
		$AudioStreamPlayer.play_sounds(["sfx_volume", "volume1"])

func _on_SFXVolume2Button_mouse_entered():
	self.focused_button = $VBoxContainer/SFXVolumeBox/Volume2Button
	if self.accessibility_mode and self.visible:
		$AudioStreamPlayer.play_sounds(["sfx_volume", "volume2"])

func _on_SFXVolumeFullButton_mouse_entered():
	self.focused_button = $VBoxContainer/SFXVolumeBox/VolumeFullButton
	if self.accessibility_mode and self.visible:
		$AudioStreamPlayer.play_sounds(["sfx_volume", "full_volume"])

func _on_CloseButton_mouse_entered():
	self.focused_button = $CloseButton
	if self.accessibility_mode and self.visible:
		$AudioStreamPlayer.play_sound("close_menu")

func _on_CloseButton_mouse_exited():
	self.focused_button = null

func _on_ControlsGrid_mouse_entered():
	if self.accessibility_mode:
		self.focused_button = $VBoxContainer/ControlsGrid
		$AudioStreamPlayer.play_sounds(["controls", "directions_controls", 
		"accept_controls", "close_menu_controls"])

func _on_Settings_visibility_changed():
	if self.accessibility_mode and self.visible:
		self.play_current_settings()
