extends Control

const kitchen_scene = preload("res://scenes/Kitchen.tscn")

var focused_button = null
onready var button_order = ["StartButton", "LevelButton", "SettingsButton", "CreditsButton"]

func _ready():
	for sound_name in ["game_title", "start", "select_recipe", "settings", "wildcard_oregano", "wildcard_duck_duck_moose", "credits"]:
		$AudioPlayer.add_sound(sound_name, "res://assets/audio/va/accessibility/%s.wav" % sound_name)
	if self.get_node("Settings").accessibility_mode:
		$AudioPlayer.play_sound("game_title")

func _on_StartButton_pressed():
	var kitchen_instance = kitchen_scene.instance()
	get_tree().get_root().add_child(kitchen_instance)
	kitchen_instance.load_recipe("Pizza")
	self.get_tree().set_current_scene(kitchen_instance)
	get_tree().get_root().remove_child(self)

func _on_LevelButton_pressed():
	get_tree().change_scene("res://scenes/SelectRecipe.tscn")

func _on_SettingsButton_pressed():
	self.get_node("Settings").show()

func move_to_node_center(node):
	self.focused_button = node
	var size = node.get_size()
	var new_loc = node.get_global_transform().get_origin() + (size / 2)
	self.get_viewport().warp_mouse(new_loc)

func _process(delta):
	if Input.is_action_just_pressed("ui_down") or Input.is_action_just_pressed("ui_right") \
	or Input.is_action_just_pressed("ui_up") or Input.is_action_just_pressed("ui_left"):
		var current_idx = self.button_order.find(self.focused_button.get_name() if self.focused_button else "")
		var new_idx = null
		if Input.is_action_just_pressed("ui_down") or Input.is_action_just_pressed("ui_right"):
			new_idx = (current_idx + 1) if (current_idx != -1) else 0
		elif Input.is_action_just_pressed("ui_up") or Input.is_action_just_pressed("ui_left"):
			new_idx = (current_idx - 1) if (current_idx != -1) else self.button_order.size() - 1
		if new_idx < 0:
			new_idx = self.button_order.size() - 1
		elif new_idx > self.button_order.size() - 1:
			new_idx = 0
		move_to_node_center(self.get_node(self.button_order[new_idx]))
	else:
		if Input.is_action_just_pressed("ui_accept") and self.focused_button:
			self.focused_button.emit_signal("pressed")

func _on_StartButton_mouse_entered():
	self.focused_button = $StartButton
	if self.get_node("Settings").accessibility_mode:
		$AudioPlayer.play_sound("start")

func _on_StartButton_mouse_exited():
	self.focused_button = null

func _on_LevelButton_mouse_entered():
	self.focused_button = $LevelButton
	if self.get_node("Settings").accessibility_mode:
		$AudioPlayer.play_sound("select_recipe")

func _on_LevelButton_mouse_exited():
	self.focused_button = null

func _on_SettingsButton_mouse_entered():
	if self.get_node("Settings").accessibility_mode:
		$AudioPlayer.play_sound("settings")

func _on_SettingsButton_mouse_exited():
	self.focused_button = null

func _on_CreditsButton_mouse_entered():
	if self.get_node("Settings").accessibility_mode:
		$AudioPlayer.play_sound("credits")

func _on_CreditsButton_mouse_exited():
	self.focused_button = null

func _on_OreganoWildcard_mouse_entered():
	if self.get_node("Settings").accessibility_mode:
		$AudioPlayer.play_sound("wildcard_oregano")

func _on_DuckDuckMooseWildcard_mouse_entered():
	if self.get_node("Settings").accessibility_mode:
		$AudioPlayer.play_sound("wildcard_duck_duck_moose")

func _on_AchievementsButton_pressed():
	get_tree().change_scene("res://scenes/AchievementScreen.tscn")

func _on_CreditsButton_pressed():
	get_tree().change_scene("res://scenes/Credits.tscn")
