extends Node

const ingredient_dict = {
	"Bacon": {
		"description": "What delicious, crispy strips! I'm always down for bacon."
	},
	"Chicken": {
		"description": "If you don't say 'tastes like chicken!', what are you even doing?"
	},
	"French Fries": {
		"description": "Why am I cooking when I could just go get fast food?"
	},
	"Ham": {
		"description": "This word also means 'to act dramatically'! How about that?"
	},
	"Pizza Crust": {
		"description": "Sure, I could make my own. But let's not do more work than we need."
	},
	"Hash Brown": {
		"description": "Potatoes are good in many forms, this form included."
	},
	"Fish": {
		"description": "I caught a salmon with my hands once! That was a fun festival."
	},
	"Waffle": {
		"description": "Waffles! Waffles waffles waffles, waffles waffles."
	},
	"Fruit Punch": {
		"description": "You're never too old for a delicious fruity drink!"
	},
	"Ketchup": {
		"description": "Almost everything tastes better with ketchup! Almost."
	},
	"Lemon Juice": {
		"description": "When life gives you lemons, make lemonade. Unless you have no sugar. Then make lemon juice."
	},
	"Pasta Sauce": {
		"description": "Mmmm, red AND delicious!"
	},
	"Cheese Sauce": {
		"description": "Could I do cheddar than this? I think not. Now I just need some nachos."
	},
	"Gravy": {
		"description": "Bring on the pot roast!"
	},
	"Hot Sauce": {
		"description": "Oooh, spicy! Not enough to scorch the fridge though..."
	},
	"Soda": {
		"description": "Bubbles! And sugar. Lots of great stuff."
	},
	"Wasabi": {
		"description": "Very spicy! Not to be confused with pistachio ice cream."
	},
	"Cheese": {
		"description": "A swisst solution to all of life's problems. Maybe not ALL of them, but I do like cheese."
	},
	"Milk": {
		"description": "A liquid addition to the dairy collection."
	},
	"Chickpeas": {
		"description": "Time to mash them up and make some hummus! Oh wait, that's not what I'm supposed to be making right now..."
	},
	"Tofu": {
		"description": "A good replacement for meat. Maybe it could replace some other things too?"
	},
	"Broccoli": {
		"description": "Ah yes, a miniature tree!"
	},
	"Cabbage": {
		"description": "Leaves, but edible! Definitely better than mutilating some random tree."
	},
	"Carrot": {
		"description": "If the rabbit can have little a carrot as a treat, so can I."
	},
	"Cauliflower": {
		"description": "It's like a mix between broccoli, lettuce, and mashed potatoes. Weird."
	},
	"Mushroom": {
		"description": "An umbrella that tastes like dirt."
	},
	"Olive": {
		"description": "A simple oval. But you get little rings when you cut it, so that's fun."
	},
	"Onion": {
		"description": "Always there for when you need a good cry."
	},
	"Bell Pepper": {
		"description": "Despite the name, this does not make a dramatic ringing when you hit it."
	},
	"Tomato": {
		"description": "For eating, or for throwing at actors who are particularly awful."
	},
	"Zucchini": {
		"description": "Not to be confused with cucumbers or pickles. Why do they all have to look so similar?"
	},
	"Packing Peanuts": {
		"description": "Random bits of styrofoam that are apparently peanut shaped?"
	},
	"Glue": {
		"description": "I mean, I suppose I am in a sticky situation..."
	},
	"Hay": {
		"description": "What do you think I am, a horse?"
	},
	"Pillow": {
		"description": "Maybe I should take a nap. Much easier than cooking."
	},
	"Bubble Wrap": {
		"description": "Cool! I'll be back in a minute, just let me go pop this. Never gets old."
	},
	"Mud": {
		"description": "And I just cleaned this table! At least it's contained to this perfectly-shaped splatter."
	},
	"Red Paint": {
		"description": "Redder than orange paint."
	},
	"Shredded Paper": {
		"description": "Quick, hide the evidence!"
	},
	"Soap": {
		"description": "Time to wash my hands of this nonsense."
	},
	"Sponge": {
		"description": "Have you absorbed my cooking skills yet?"
	},
	"Tennis Ball": {
		"description": "Not sure how this got on the table. I blame the dog."
	},
	"Toothpaste": {
		"description": "Always practice good dental hygeine. Remember, brush your teeth AFTER eating, not before!"
	},
	"Yarn": {
		"description": "I keep thinking I'll do more knitting, but I never get around to it. Surprised the cat hasn't made off with this yet."
	},
	"Black Pepper": {
		"description": "Good for seasoning, or for making cartoon characters sneeze."
	},
	"Butter": {
		"description": "Smooth as, well... butter, but I'm sure you knew that already."
	},
	"Flour": {
		"description": "A baking material. Not to be confused with petal-bearing plants."
	},
	"Garlic": {
		"description": "Take THAT, vampires!"
	},
	"Macaroni": {
		"description": "Stuck a feather in his cap and called it yummy pasta!"
	},
	"Olive Oil": {
		"description": "Drizzle a little of this on your dish to make it all sophisticated."
	},
	"Soy Sauce": {
		"description": "I didn't think a condiment could be that salty, but there it is!"
	},
	"Oregano": {
		"description": "Everything is better with oregano!"
	},
	"Potato": {
		"description": "Boil em, mash em, stick em in a stew."
	},
	"Seaweed": {
		"description": "I would just grab some from the sea, but it's probably supposed to go through some kind of processing first."
	},
	"Strawberry Jam": {
		"description": "Basically just fruit and sugar. Marvelous!"
	},
	"Sugar": {
		"description": "Sweet!"
	},
	"Salt": {
		"description": "Just in case I wasn't already salty enough about all this."
	},
	"Wheat": {
		"description": "Allergy warning: this product may contain wheat."
	},
	"Banana": {
		"description": "Loved by monkeys and humans alike! Just be careful where you leave the peel."
	},
	"Coconut": {
		"description": "With how strange some of these substitutions are, I wonder if the developer was hit on the head by one of these."
	},
	"Coffee": {
		"description": "Bean juice for when you need that extra boost of speed! It's probably cold by now."
	},
	"Corn": {
		"description": "Ripping the husk off is very therapeutic."
	},
	"Frittata": {
		"description": "Wait, why am I not just eating these leftovers?"
	},
	"Pineapple": {
		"description": "Spiky on the outside, sweet on the core. Like a lot of people. But don't eat people."
	},
	"Popcorn": {
		"description": "Always ready for when my favorite cooking shows get dramatic!"
	},
	"Mashed Potatoes": {
		"description": "Oh hey, these are conveniently ready to go! Smashing!"
	},
	"Pumpkin": {
		"description": "Who said Pumpkins are just for fall? Nature? Well, that's lame."
	},
	"Water": {
		"description": "Never underestimate something so crucial to life."
	},
	"Lava Lamp": {
		"description": "Oooooh, look at the mesmerizing flowy thing!"
	}
}
