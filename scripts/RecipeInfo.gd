extends Node

var rng = RandomNumberGenerator.new()

var discovery = {
	"Pizza": {
		"extra": [],
		"simple": [],
		"weird": [],
		"combination": false,
		"non-food": false,
		"non-food combination": false
	},
	"Soup": {
		"extra": [],
		"simple": [],
		"weird": [],
		"combination": false,
		"non-food": false,
		"non-food combination": false
	},
	"Sandwich": {
		"extra": [],
		"simple": [],
		"weird": [],
		"combination": false,
		"non-food": false,
		"non-food combination": false
	},
	"Spaghetti": {
		"extra": [],
		"simple": [],
		"weird": [],
		"combination": false,
		"non-food": false,
		"non-food combination": false
	},
	"Omelette": {
		"extra": [],
		"simple": [],
		"weird": [],
		"combination": false,
		"non-food": false,
		"non-food combination": false
	},
	"Sushi": {
		"extra": [],
		"simple": [],
		"weird": [],
		"combination": false,
		"non-food": false,
		"non-food combination": false
	},
	"Mac and Cheese": {
		"extra": [],
		"simple": [],
		"weird": [],
		"combination": false,
		"non-food": false,
		"non-food combination": false
	},
	"ingredients": []
}

var discovery_file = "user://discovery.save"

const recipes = [
	{
		"title": "Pizza",
		"missing": "Sauce",
		"remove": ["Pasta Sauce"],
		"existing": ["Pizza Crust", "Cheese", "Bacon"],
		"optional": ["Ham", "Pineapple", "Mushroom", "Olive", "Onion", "Garlic", "Bell Pepper", "Oregano"],
		"simple": ["Cheese Sauce", "Tomato", "Hot Sauce"],
		"weird": ["Strawberry Jam", "Gravy", "Soy Sauce"],
		"combination": ["Ketchup", "Oregano"],
		"non-food": ["Lava Lamp"],
		"non-food combination": ["Red Paint", "Tomato"],
		"hints": {
			"Ketchup": "Good tomato flavor, but needs some seasoning. What else goes on pizza?",
			"Oregano": "Always tasty, but not very saucy. Can I mix this with some kind of sauce consistency?",
			"Red Paint": "It's the right color, but it tastes nothing like pizza. Not exactly surprising.",
			"Tomato": "Yay, tomato! I need some more liquid though."
		},
		"comments": {
			"Strawberry Jam": "I guess it's dessert pizza now?",
			"Gravy": "Actually that sounds pretty good! Not for a veggie pizza though.",
			"Soy Sauce": "I'm kind of surprised it didn't all evaporate it's so liquidy.",
			"Lava Lamp": "I don't know why, but it lava lamps just feel warm and inviting like a good pizza.",
			"combination": "Oregano makes everything better! And so does ketchup. Power couple!",
			"non-food combination": "That must be a really good tomato to cover up the paint taste."
		},
		"start": "Ah yes, a classic! ...Oh. That's not good. And no, I am not going to make none pizza with left beef. I'm too hungry to meme. I'll just have to find a substitute for that sauce."
	},
	{
		"title": "Soup",
		"missing": "Water",
		"existing": ["Carrot", "Onion", "Garlic", "Chicken", "Salt"],
		"optional": ["Tomato", "Potato", "Corn", "Broccoli", "Bacon", "Black Pepper", "Oregano", "Fish"],
		"simple": ["Pasta Sauce", "Cheese Sauce", "Milk"],
		"weird": ["Soda", "Fruit Juice", "Coffee"],
		"combination": ["Black Pepper", "Lemon Juice"],
		"non-food": ["Glue"],
		"non-food combination": ["Oregano", "Soap"],
		"hints": {
			"Lemon Juice": "Nice and liquidy, but really sour! Can I add any savory flavors?",
			"Black Pepper": "This brings out the flavor of the soup, but, uh... it's not a liquid. Not at all.",
			"Soap": "Liquid soap works, but it's gonna take something truly delicious to make this palatable.",
			"Oregano": "Always a desirable flavor, but something's off. Can I clean this up a bit?"
		},
		"comments": {
			"Soda": "Fizzy soup! This is a new experience.",
			"Fruit Juice": "Dessert soup? I mean, sweet + salty isn't the worst combo ever...",
			"Coffee": "Well if that ridiculous idea didn't wake me up, I guess my soup will now.",
			"combination": "You know, that almost sounds normal!",
			"Glue": "Now the soup is stuck to the roof of my mouth!",
			"non-food combination": "Oregano will make things clean and soap tastes good. Wait, that's not right!"
		},
		"start": "Of all the times for the water to get shut off! I mean, I could do stew, or casserole... eh, who am I kidding, I'm too stubborn for that! Weird soup it is."
	},
		{
		"title": "Sandwich",
		"missing": "Bread",
		"remove": ["Wheat"],
		"existing": ["Bacon", "Cabbage", "Tomato"],
		"optional": ["Ham", "Cheese", "Oregano", "Mushroom", "Ketchup", "Fish"],
		"simple": ["Pizza Crust", "Waffle", "Hash Brown"],
		"weird": ["Frittata", "Potato", "Tofu"],
		"combination": ["Butter", "Flour", "Salt", "Sugar", "Water"],
		"non-food": ["Pillow"],
		"non-food combination": ["Salt", "Sponge", "Sugar"],
		"hints": {
			"Water": "Maybe if I mixed this with some other stuff I could just make my own bread!",
			"Salt": "Maybe if I mixed this with some other stuff I could just make my own bread! Or something like bread.",
			"Sugar": "Maybe if I mixed this with some other stuff I could just make my own bread! Or something like bread.",
			"Butter": "Maybe if I mixed this with some other stuff I could just make my own bread!",
			"Flour": "Maybe if I mixed this with some other stuff I could just make my own bread!",
			"Sponge": "The texture is kind of like bread, but this tastes awful. What goes into making bread?"
		},
		"comments": {
			"Frittata": "I mean, I could just eat that instead of adding it to a sandwich, but okay.",
			"Potato": "So basically a jacket potato but I just call it a sandwich.",
			"Tofu": "I'd say this is the vegetarian option, but bread is already vegetarian.",
			"Pillow": "Have you ever dreamed you were eating your pillow? It's cause pillows are great sandwich material.",
			"combination": "So, I just... made bread? What is this, normal cooking?",
			"non-food combination": "Wait long enough, and this knock-off bread will have absorbed all the ingredients!"
		},
		"start": "Ok, this is getting kind of ridiculous. How am I consistently missing fundamental recipe ingredients? Is my luck just really that awful?"
	},
	{
		"title": "Spaghetti",
		"missing": "Noodles",
		"remove": ["Macaroni"],
		"existing": ["Pasta Sauce", "Cheese", "Chicken"],
		"optional": ["Oregano", "Salt", "Black Pepper", "Onion", "Garlic", "Bacon"],
		"simple": ["Zucchini", "Carrot", "Cabbage"],
		"weird": ["French Fries", "Potato", "Broccoli"],
		"combination": ["Corn", "Flour"],
		"non-food": ["Yarn"],
		"non-food combination": ["Hay", "Milk"],
		"hints": {
			"Flour": "I wonder if I could use this to coat something that looks kind of like noodles?",
			"Corn": "Um, maybe this looks like noodles? I wanna put something on it though.",
			"Hay": "I guess you could call that spaghetti-shaped, but this is way too dry.",
			"Milk": "Definitely not noodles, but it might mix with something else."
		},
		"comments": {
			"French Fries": "Delicious starch. Check! Long-ish strips. Check!",
			"Potato": "That work was so grating! At least it tastes good.",
			"Broccoli": "Tree-noodles! That's a phrase I never would have imagined I'd say.",
			"combination": "These are the weirdest homemade noodles I've ever made, and that's kind of a high bar.",
			"Yarn": "The taste is... indescribable.",
			"non-food combination": "Noodles, but for horses!"
		},
		"start": "Can I really call this spaghetti if I don't have spaghetti? Whatever, it's not like I'm making champagne."
	},
	{
		"title": "Omelette",
		"missing": "Eggs",
		"existing": ["Cheese", "Tomato", "Salt"],
		"optional": ["Onion", "Bell Pepper", "Black Pepper", "Mushroom", "Garlic", "Chicken", "Ham", "Bacon", "Ketchup", "Oregano"],
		"simple": ["Tofu", "Flour", "Chickpea"],
		"weird": ["Banana", "Mashed Potatoes", "Pumpkin"],
		"combination": ["Corn", "Water"],
		"non-food": ["Bubble Wrap"],
		"non-food combination": ["Lemon Juice", "Tennis Ball"],
		"hints": {
			"Water": "Eggs have a bit more substance than just liquid. But this is a good start.",
			"Corn": "I can mash this up into a paste, but the paste is still more solid than egg.",
			"Tennis Ball": "Yeah, that's kind of egg-shaped. But it's dry and tastes awful.",
			"Lemon Juice": "Liquid is good. Is there anything that looks like an egg that I can mix this with?"
		},
		"comments": {
			"Banana": "Mushy. Maybe a bit like eggs? Close enough.",
			"Mashed Potatoes": "I don't think it can be called an omelette anymore. But I'd eat it.",
			"Pumpkin": "Halloween omelette! This pumpkin would probably do better as a Jack-o-Lantern though.",
			"combination": "I guess corn is useful for something other than getting stuck in my teeth.",
			"Bubble Wrap": "Well now I just wanna pop the omelette! That would probably make a mess.",
			"non-food combination": "I can't really bite into this, but we'll call it success anyway."
		},
		"start": "Just yesterday I had a bunch of double-yolked eggs from the egg vending machine! How are they all gone already?"
	},
	{
		"title": "Sushi",
		"missing": "Rice",
		"existing": ["Fish", "Seaweed"],
		"optional": ["Soy Sauce", "Wasabi", "Oregano"],
		"simple": ["Cauliflower", "Broccoli", "Wheat"],
		"weird": ["Cheese", "Mashed Potatoes", "Popcorn"],
		"combination": ["Butter", "Coconut"],
		"non-food": ["Packing Peanuts"],
		"non-food combination": ["Butter", "Shredded Paper"],
		"hints": {
			"Coconut": "This is white and I can shred it like rice. But the pieces just aren't sticking together.",
			"Butter": "Hey, this is sticky like sushi rice! But it's not stuck to anything...",
			"Shredded Paper": "Lots of little white pieces. Looks kind of like rice. But it's not a good taste."
		},
		"comments": {
			"Cheese": "I guess you can grate just about anything and call it rice.",
			"Mashed Potatoes": "If I pretend hard enough, I can imagine it's real rice!",
			"Popcorn": "I mean, it's sort of grainy I guess? Not sure why I thought this was a good idea.",
			"combination": "Coconut sushi! Makes me wanna go to the beach.",
			"Packing Peanuts": "It's disturbing how close these are to edible.",
			"non-food combination": "I think we can say the evidence is thoroughly hidden."
		},
		"start": "I don't know what abomination I'm about to make, but I think if I saw it on a sushi conveyor belt I'd get the heck out of there."
	},
	{
		"title": "Mac and Cheese",
		"missing": "Milk",
		"remove": ["Cheese"],
		"existing": ["Macaroni", "Cheese", "Butter", "Flour"],
		"optional": ["Bacon", "Black Pepper", "Oregano"],
		"simple": ["Water", "Cheese Sauce", "Lemon Juice"],
		"weird": ["Olive Oil", "Gravy", "Hot Sauce"],
		"combination": ["Cheese Sauce", "Pasta Sauce"],
		"non-food": ["Mud"],
		"non-food combination": ["Toothpaste", "Water"],
		"hints": {
			"Pasta Sauce": "The sauce is a good consistency, but it overpowers the cheese flavor!",
			"Toothpaste": "This might work, but it's too thick."
		},
		"comments": {
			"Olive Oil": "Don't try this at home. No, seriously. I tried once, and the taste haunts me to this day.",
			"Gravy": "Is this the pasta version of poutine?",
			"Hot Sauce": "I have invented spicy mac and cheese! I mean, it probably already exists, but I made some now.",
			"combination": "So much sauce. So much.",
			"Mud": "I guess I really bit the dust on this one, huh?",
			"non-food combination": "I wouldn't recommend eating toothpaste, but this was better than I expected."
		},
		"start": "Ok, I've dealt with this before. I better not make the same mistake I did then, cause that was the most disgusting thing I've ever eaten, recent inventions included."
	}
]

const fail_messages = ["No.", "Nope!", "Absolutely not!", "Ewwww!", "Eugh!", "Gross!",
"Blech!", "Hahahahahahaha... no.", "Mmmmm... nope.", "Well, that was certainly a thing.",
"I'm not making that mistake again!", "Sorry, I'm on a strict no-poison diet.",
"Look, I gotta draw the line somewhere.", "I just remembered I have to be somewhere else. Immediately.",
"I... Wow... it's impressive how atrocious that is.", "Yeah, I'm gonna pass.", "I wouldn't even give that to a taste-tester!",
"It belongs in a museum exhibit: meals to definitely never eat.",
"The terrible, horrible, no-good, very bad food.", "Sorry, my jaw won't open to eat this.",
"I wouldn't touch that with a ten-foot spatula!"]
const success_messages = ["Ok.", "Yeah, I guess that'll do.", "A little boring, but I'll take it.",
"Alright.", "Yeah, that's edible.", "Acceptable.", "Not my favorite, but it does the trick.",
"Playing it safe, I see?", "I'd eat that.", "If you insist.", "Here's to me not starving!",
"Sure.", "Adequate.", "The quality is sufficient.", "Well, that doesn't completely destroy my appetite.",
"Not completely terrible.", "I suppose I could eat it.", "Fine.", "Nice try, I'll allow it.",
"That could be eaten, maybe even by me!"]

func _ready():
	load_discovery_file()
	rng.randomize()

func evaluate_recipe(recipe_title, extra_items, sub_items):
	var info = null
	for recipe in recipes:
		if recipe["title"] == recipe_title:
			info = recipe
			break
	var result = {}
	result["good_extras"] = []
	result["bad_extras"] = []
	result["sub_ok"] = false
	result["hint"] = ""
	result["comment"] = ""
	result["audio_name"] = ""
	var recipe_title_lower = recipe_title.replace(" ", "").to_lower()
	for item in extra_items:
		if item in info["optional"]:
			result["good_extras"].append(item)
			if !(item in discovery[recipe_title]["extra"]):
				if !(item in discovery["ingredients"]):
					AchievementManager.progress_achievement("other_ingredients", 1)
					discovery["ingredients"].append(item)
				AchievementManager.progress_achievement("%s_extras" % recipe_title_lower, 1)
				AchievementManager.progress_achievement("other_extras", 1)
				discovery[recipe_title]["extra"].append(item)
		else:
			result["bad_extras"].append(item)
	if "Oregano" in extra_items:
		check_oregano_achievement()
	if sub_items.size() == 1:
		var single_sub = sub_items[0]
		var single_sub_lower = single_sub.replace(" ", "").to_lower()
		if single_sub in info["simple"]:
			result["sub_ok"] = true
			if !(single_sub in discovery[recipe_title]["simple"]):
				discovery[recipe_title]["simple"].append(single_sub)
				AchievementManager.unlock_achievement("%s_sub_%s" % [recipe_title_lower, single_sub_lower])
				AchievementManager.progress_achievement("%s_subs" % recipe_title_lower, 1)
				AchievementManager.progress_achievement("other_subs", 1)
		elif single_sub in info["weird"] or single_sub in info["non-food"]:
			result["sub_ok"] = true
			result["comment"] = info["comments"][single_sub]
			result["audio_name"] = "%s_comments_%s" % [recipe_title, single_sub]
			if single_sub in info["weird"]:
				if !(single_sub in discovery[recipe_title]["weird"]):
					discovery[recipe_title]["weird"].append(single_sub)
					AchievementManager.unlock_achievement("%s_sub_%s" % [recipe_title_lower, single_sub_lower])
					AchievementManager.progress_achievement("%s_subs" % recipe_title_lower, 1)
					AchievementManager.progress_achievement("other_subs", 1)
			elif single_sub in info["non-food"]:
				AchievementManager.unlock_achievement("%s_sub_%s" % [recipe_title_lower, single_sub_lower])
				if !discovery[recipe_title]["non-food"]:
					discovery[recipe_title]["non-food"] = true
					AchievementManager.progress_achievement("%s_subs" % recipe_title_lower, 1)
					AchievementManager.progress_achievement("other_subs", 1)
					AchievementManager.progress_achievement("other_nonfood", 1)
				print("unlocked non-food sub: %s_sub_%s" % [recipe_title_lower, single_sub])
	else:
		var subs = sub_items.duplicate()
		subs.sort()
		var subs_lower = subs[0].replace(" ", "").to_lower()
		for item in subs.slice(1, len(subs)):
			subs_lower += "_" + item.replace(" ", "").to_lower()
		if subs == info["combination"]:
			result["sub_ok"] = true
			result["comment"] = info["comments"]["combination"]
			result["audio_name"] = "%s_comments_combination" % recipe_title
			if !discovery[recipe_title]["combination"]:
				discovery[recipe_title]["combination"] = true
				AchievementManager.unlock_achievement("%s_sub_%s" % [recipe_title_lower, subs_lower])
				AchievementManager.progress_achievement("%s_subs" % recipe_title_lower, 1)
				AchievementManager.progress_achievement("other_combination", 1)
			print("unlocked combination sub: %s_sub_%s" % [recipe_title_lower, subs_lower])
		elif subs == info["non-food combination"]:
			result["sub_ok"] = true
			result["comment"] = info["comments"]["non-food combination"]
			result["audio_name"] = "%s_comments_non-food combination" % recipe_title
			if !discovery[recipe_title]["non-food combination"]:
				discovery[recipe_title]["non-food combination"] = true
				AchievementManager.unlock_achievement("%s_sub_%s" % [recipe_title_lower, subs_lower])
				AchievementManager.progress_achievement("%s_subs" % recipe_title_lower, 1)
				AchievementManager.progress_achievement("other_nonfood", 1)
			print("unlocked non-food combination sub: %s_sub_%s" % [recipe_title_lower, subs_lower])
	if result["sub_ok"]:
		for item in sub_items:
			if !(item in discovery["ingredients"]):
				AchievementManager.progress_achievement("other_ingredients", 1)
				discovery["ingredients"].append(item)
		if !result["comment"]:
			var i = rng.randi_range(0, 20)
			result["comment"] = self.success_messages[i]
			result["audio_name"] = "yes_%d" % i
	else:
		var bad_sub = false
		for item in sub_items:
			if item in info["hints"]:
				result["hint"] = info["hints"][item]
				result["audio_name"] = "%s_hints_%s" % [recipe_title, item]
			else:
				bad_sub = true
		if bad_sub and result["hint"]:
			result["comment"] = "There's something promising in here, but I don't like this combination."
			result["audio_name"] = ""
		elif bad_sub:
			var i = rng.randi_range(0, 20)
			result["comment"] = self.fail_messages[i]
			result["audio_name"] = "no_%d" % i
	update_discovery_file()
	return result

func update_discovery_file():
	var file = File.new()
	file.open(discovery_file, File.WRITE)
	file.store_var(discovery, true)
	file.close()

func check_oregano_achievement():
	var all_oregano = true
	for recipe in discovery:
		if recipe == "ingredients":
			continue
		if !("Oregano" in discovery[recipe]["extra"]):
			all_oregano = false
			break
	if all_oregano:
		AchievementManager.unlock_achievement("other_oregano")

func load_discovery_file():
	var file = File.new()
	if file.file_exists(discovery_file):
		file.open(discovery_file, File.READ)
		discovery = file.get_var(true)
		file.close()
