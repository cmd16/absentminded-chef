extends Control

var current_title = null
var focused_button = null
const kitchen_scene = preload("res://scenes/Kitchen.tscn")
var accessibility_mode = true
var settings_file = "user://settings.save"

# Called when the node enters the scene tree for the first time.
func _ready():
	for sound_name in ["Pizza", "Soup", "Sandwich", "Spaghetti", "Omelette", "Sushi", "Mac and Cheese",
	"back_to_start", "make_recipe", "cancel", "extra_ingredients", "simple_sub", 
	"weird_sub", "combination", "non-food", "non-food_combination", "unknown", 
	"0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "of", "select_recipe"]:
		$AudioStreamPlayer.add_sound(sound_name, "res://assets/audio/va/accessibility/%s.wav" % sound_name)
	var file = File.new()
	if file.file_exists(settings_file):
		file.open(settings_file, file.READ)
		self.accessibility_mode = file.get_var()
	if self.accessibility_mode:
		$AudioStreamPlayer.play_sound("select_recipe")
	var acc_dir = Directory.new()
	var path = "res://assets/audio/va/Ingredients"
	acc_dir.open(path)
	acc_dir.list_dir_begin()
	while true:
		var sound_file = acc_dir.get_next()
		if sound_file == "":
			break
		if sound_file.ends_with("title.wav"):
			$AudioStreamPlayer.add_sound(sound_file.replace(".wav", ""), "%s/%s" % [path, sound_file])

func popup_recipe(title):
	var recNode = self.get_node("RecipePopup")
	recNode.get_node("TitleLabel").text = title
	var vboxNode = recNode.get_node("ScrollContainer/VBoxContainer")
	self.current_title = title
	var discoveryInfo = self.get_node("RecipeInfo").discovery[title]
	var rec = null
	for item in self.get_node("RecipeInfo").recipes:
		if item["title"] == title:
			rec = item
			break
	vboxNode.get_node("ExtraLabel").text = "Extra ingredients: %s (%d/%d)" % [PoolStringArray(discoveryInfo["extra"]).join(", "), discoveryInfo["extra"].size(), rec["optional"].size()]
	vboxNode.get_node("SimpleLabel").text = "Simple substitutions: %s (%d/%d)" % [PoolStringArray(discoveryInfo["simple"]).join(", "), discoveryInfo["simple"].size(), rec["simple"].size()]
	vboxNode.get_node("WeirdLabel").text = "Weird substitutions: %s (%d/%d)" % [PoolStringArray(discoveryInfo["weird"]).join(", "), discoveryInfo["weird"].size(), rec["weird"].size()]
	vboxNode.get_node("CombinationLabel").text = "Combination: %s" % (PoolStringArray(rec["combination"]).join(", ") if discoveryInfo["combination"] else "unknown")
	vboxNode.get_node("NonfoodLabel").text = "Non-food: %s" % (rec["non-food"] if discoveryInfo["non-food"] else "unknown")
	vboxNode.get_node("NonfoodCombinationLabel").text = "Food and non-food combination: %s" % (PoolStringArray(rec["non-food combination"]).join(", ") if discoveryInfo["non-food combination"] else "unknown")
	recNode.popup_centered()
	if self.accessibility_mode:
		var sound_names = [title, "extra_ingredients"]
		for item in discoveryInfo["extra"]:
			sound_names.append("%s_title" % item)
		for item in ["%d" % discoveryInfo["extra"].size(), "of", "%d" % rec["optional"].size()]:
			sound_names.append(item)
		sound_names.append("simple_sub")
		for item in discoveryInfo["simple"]:
			sound_names.append("%s_title" % item)
		for item in ["%d" % discoveryInfo["simple"].size(), "of", "%d" % rec["simple"].size()]:
			sound_names.append(item)
		sound_names.append("weird_sub")
		for item in discoveryInfo["weird"]:
			sound_names.append("%s_title" % item)
		for item in ["%d" % discoveryInfo["weird"].size(), "of", "%d" % rec["weird"].size()]:
			sound_names.append(item)
		sound_names.append("combination")
		if discoveryInfo["combination"]:
			for item in discoveryInfo["combination"]:
				sound_names.append("%s_title" % item)
		else:
			sound_names.append("unknown")
		sound_names.append("non-food")
		if discoveryInfo["non-food"]:
			for item in discoveryInfo["non-food"]:
				sound_names.append("%s_title" % item)
		else:
			sound_names.append("unknown")
		sound_names.append("non-food_combination")
		if discoveryInfo["non-food combination"]:
			for item in discoveryInfo["non-food combination"]:
				sound_names.append("%s_title" % item)
		else:
			sound_names.append("unknown")
		$AudioStreamPlayer.play_sounds(sound_names)

func _on_PizzaButton_pressed():
	popup_recipe("Pizza")

func _on_SoupButton_pressed():
	popup_recipe("Soup")

func _on_SandwichButton_pressed():
	popup_recipe("Sandwich")

func _on_SpaghettiButton_pressed():
	popup_recipe("Spaghetti")

func _on_OmeletteButton_pressed():
	popup_recipe("Omelette")

func _on_SushiButton_pressed():
	popup_recipe("Sushi")

func _on_MacandCheeseButton_pressed():
	popup_recipe("Mac and Cheese")

func _on_StartButton_pressed():
	var kitchen_instance = kitchen_scene.instance()
	get_tree().get_root().add_child(kitchen_instance)
	kitchen_instance.load_recipe(self.current_title)
	get_tree().get_root().remove_child(self)

func _on_CancelButton_pressed():
	self.get_node("RecipePopup").hide()

func _on_BackButton_pressed():
	get_tree().change_scene("res://scenes/Start.tscn")

func move_to_node_center(node):
	self.focused_button = node
	var size = node.get_size()
	var new_loc = node.get_global_transform().get_origin() + (size / 2)
	self.get_viewport().warp_mouse(new_loc)
	node.emit_signal("mouse_entered")

func _process(delta):
	if Input.is_action_just_pressed("ui_down") or Input.is_action_just_pressed("ui_right") \
	or Input.is_action_just_pressed("ui_up") or Input.is_action_just_pressed("ui_left"):
		if $RecipePopup.visible:
			if !self.focused_button and (Input.is_action_just_pressed("ui_left") or Input.is_action_just_pressed("ui_up")) \
			or self.focused_button == $RecipePopup/StartButton:
				self.move_to_node_center($RecipePopup/CancelButton)
			else:
				self.move_to_node_center($RecipePopup/StartButton)
		else:
			var current_idx = $GridContainer.get_children().find(self.focused_button)
			var new_idx = null
			if Input.is_action_just_pressed("ui_right"):
				new_idx = (current_idx + 1) if (current_idx != -1) else 0
			elif Input.is_action_just_pressed("ui_left"):
				new_idx = (current_idx - 1) if (current_idx != -1) else $GridContainer.get_child_count() - 1
			elif Input.is_action_just_pressed("ui_up"):
				new_idx = (current_idx - $GridContainer.columns) if (current_idx != -1) else 0
			elif Input.is_action_just_pressed("ui_down"):
				new_idx = (current_idx + $GridContainer.columns) if (current_idx != -1) else $GridContainer.get_child_count() - 1
			if new_idx < 0:
				new_idx = $GridContainer.get_child_count() - 1
			elif new_idx > $GridContainer.get_child_count() - 1:
				if current_idx == $GridContainer.get_child_count() - 1:
					new_idx = 0
				else:
					new_idx = $GridContainer.get_child_count() - 1
			if current_idx == $GridContainer.get_child_count() - 1 and Input.is_action_just_pressed("ui_down"):
				move_to_node_center($BackButton)
			elif current_idx == 0 and Input.is_action_just_pressed("ui_up"):
				move_to_node_center($BackButton)
			elif current_idx == -1:
				if Input.is_action_just_pressed("ui_up") or Input.is_action_just_pressed("ui_left"):
					move_to_node_center($GridContainer/MacandCheeseButton)
				else:
					move_to_node_center($GridContainer/PizzaButton)
			else:
				move_to_node_center($GridContainer.get_child(new_idx))
	
	if Input.is_action_just_pressed("ui_accept") and self.focused_button:
			self.focused_button.emit_signal("pressed")

func _on_PizzaButton_mouse_entered():
	self.focused_button = $GridContainer/PizzaButton
	if self.accessibility_mode:
		$AudioStreamPlayer.play_sound("Pizza")

func _on_PizzaButton_mouse_exited():
	self.focused_button = null

func _on_SoupButton_mouse_entered():
	self.focused_button = $GridContainer/SoupButton
	if self.accessibility_mode:
		$AudioStreamPlayer.play_sound("Soup")

func _on_SoupButton_mouse_exited():
	self.focused_button = null

func _on_SandwichButton_mouse_entered():
	self.focused_button = $GridContainer/SandwichButton
	if self.accessibility_mode:
		$AudioStreamPlayer.play_sound("Sandwich")

func _on_SandwichButton_mouse_exited():
	self.focused_button = null

func _on_SpaghettiButton_mouse_entered():
	self.focused_button = $GridContainer/SpaghettiButton
	if self.accessibility_mode:
		$AudioStreamPlayer.play_sound("Spaghetti")

func _on_SpaghettiButton_mouse_exited():
	self.focused_button = null

func _on_OmeletteButton_mouse_entered():
	self.focused_button = $GridContainer/OmeletteButton
	if self.accessibility_mode:
		$AudioStreamPlayer.play_sound("Omelette")

func _on_OmeletteButton_mouse_exited():
	self.focused_button = null

func _on_SushiButton_mouse_entered():
	self.focused_button = $GridContainer/SushiButton
	if self.accessibility_mode:
		$AudioStreamPlayer.play_sound("Sushi")

func _on_SushiButton_mouse_exited():
	self.focused_button = null

func _on_MacandCheeseButton_mouse_entered():
	self.focused_button = $GridContainer/MacandCheeseButton
	if self.accessibility_mode:
		$AudioStreamPlayer.play_sound("Mac and Cheese")

func _on_MacandCheeseButton_mouse_exited():
	self.focused_button = null

func _on_BackButton_mouse_entered():
	self.focused_button = $BackButton
	if self.accessibility_mode:
		$AudioStreamPlayer.play_sound("back_to_start")

func _on_BackButton_mouse_exited():
	self.focused_button = null

func _on_StartButton_mouse_entered():
	self.focused_button = $RecipePopup/StartButton
	if self.accessibility_mode:
		$AudioStreamPlayer.play_sound("make_recipe")

func _on_StartButton_mouse_exited():
	self.focused_button = null

func _on_CancelButton_mouse_entered():
	self.focused_button = $RecipePopup/CancelButton
	if self.accessibility_mode:
		$AudioStreamPlayer.play_sound("cancel")

func _on_CancelButton_mouse_exited():
	self.focused_button = null
